import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from  './views/home/home.component';
import { EmployeesComponent } from './views/employees/employees.component';
import { EmployeesCreateComponent } from  './components/employees/employees-create/employees-create.component';
import {EmployeesUpdateComponent} from './components/employees/employees-update/employees-update.component';

const routes: Routes = [
  {path: "",
  component: HomeComponent
  },
  {
   path: "employees",
   component: EmployeesComponent
  },
  {
    path: "employees/create",
    component: EmployeesCreateComponent
  },
  {
    path: "employees/update/:id",
    component: EmployeesUpdateComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
