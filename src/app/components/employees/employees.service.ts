import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Employee } from './employees.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {

  baseUrl = "http://127.0.0.1:8000/api/addEmployee"

  constructor(private snackBar:MatSnackBar, private http:HttpClient) { }

  showOnConsole(message: string) : void {
    this.snackBar.open(message, '', {
      duration: 3000,
      horizontalPosition: "right",
      verticalPosition: "top"
    })
  }

  create(employee: Employee): Observable<Employee>{
    return this.http.post<Employee>(this.baseUrl, employee);
  }

  read(): Observable<Employee[]> {
    return this.http.get<Employee[]>('http://127.0.0.1:8000/api/employees');
  }

  readbyID(id: string): Observable<Employee> {
    return this.http.get<Employee>('http://127.0.0.1:8000/api/employee/'+id);
  }

  update(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>('http://127.0.0.1:8000/api/updateEmployee/'+ employee.id, employee);
  }

  deletebyID(id: string): Observable<Employee> {
    return this.http.delete<Employee>('http://127.0.0.1:8000/api/deleteEmployee/'+id);
  }
}
