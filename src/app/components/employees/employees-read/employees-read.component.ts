import { Component, OnInit } from '@angular/core';
import {EmployeesService} from '../employees.service';
import {Employee} from '../employees.model';
import { ActivatedRoute, Router } from  '@angular/router';

@Component({
  selector: 'app-employees-read',
  templateUrl: './employees-read.component.html',
  styleUrls: ['./employees-read.component.css']
})
export class EmployeesReadComponent implements OnInit {

  constructor(private employeesService: EmployeesService, private route:ActivatedRoute, private router:Router) { 
    this.employees = [];
  }

  employees: Employee[];
  displayedColumns = ['id', 'name', 'salary','action'];

  ngOnInit(): void {
    console.log(this.employeesService.read().subscribe(employeess => {
      this.employees = employeess
      console.log(this.employees)
    }));
  }

  deleteById(id : string): void {
    
    if (id !== null) {
      this.employeesService.deletebyID(id).subscribe( employeeId => {
        this.employeesService.showOnConsole('Empregado deletado com sucesso')
        this.reloadComponent();
      });
    }
  }

  //Função para recarregar o componente
  reloadComponent() {
    let currentUrl = this.router.url;
        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate([currentUrl]);
    }

}
