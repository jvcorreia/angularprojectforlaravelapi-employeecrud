import { Component, OnInit } from '@angular/core';
import { EmployeesService } from '../employees.service';
import { Router } from  '@angular/router';
import { Employee } from '../employees.model';

@Component({
  selector: 'app-employees-create',
  templateUrl: './employees-create.component.html',
  styleUrls: ['./employees-create.component.css']
})
export class EmployeesCreateComponent implements OnInit {

  employee: Employee = {
    name: "",
    email: "",
    salary: 0
  }
  atributoLegal = "qualquer!"

  constructor(private employeesService: EmployeesService, private router: Router) {


   }

  ngOnInit(): void {
   
  }

  fazerAlgo(): void {
    console.log("Fazendo algo......")
  }

  createEmployee(): void {
    this.employeesService.create(this.employee).subscribe(() =>{
      this.employeesService.showOnConsole('Empregado criado com sucesso')
      this.router.navigate(['/employees'])

    })
    
  }

  cancel(): void {
    this.router.navigate(['/employees'])
  }

}
