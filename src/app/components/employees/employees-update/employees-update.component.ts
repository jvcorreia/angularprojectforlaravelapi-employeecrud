import { Component, OnInit } from '@angular/core';
import {EmployeesService} from '../employees.service';
import {Employee} from '../employees.model';
import { ActivatedRoute, Router } from  '@angular/router';


@Component({
  selector: 'app-employees-update',
  templateUrl: './employees-update.component.html',
  styleUrls: ['./employees-update.component.css']
})
export class EmployeesUpdateComponent implements OnInit {

  constructor(private employeesService: EmployeesService,
     private router:Router, private route:ActivatedRoute) { 
  
  }
  
  employee: Employee = {
    name: "",
    email: "",
    salary: 0
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    if (id !== null) {
      this.employeesService.readbyID(id).subscribe( employeeId =>{
        this.employee = employeeId;
        console.log(this.employee)
      })
    }else {
      this.employeesService.showOnConsole('O id que você tentou acessar não existe')
      this.router.navigate(['/employees'])
    }
    
  }

  updateProduct(): void {
    this.employeesService.update(this.employee).subscribe((employeeId: Employee) =>{
      this.employeesService.showOnConsole('Empregado Atualizado com sucesso')
      this.router.navigate(['/employees'])
    })
  }

  cancel(): void {
    this.router.navigate(['/employees'])
  }

}
